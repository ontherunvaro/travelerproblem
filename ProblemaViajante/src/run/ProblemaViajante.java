package run;

import java.util.List;
import java.util.Scanner;

import model.LowerTriangularMatrix;
import model.TravelerOperations;
import util.EmbeddedRandom;
import util.FileRandom;
import util.MatrixLoader;
import util.RandomProvider;

public class ProblemaViajante {

	private static final String FILE_NAME = "res/distancias_10.txt";
	private static final Integer SIZE = 10;

	public static void main(String[] args) {

		LowerTriangularMatrix<Integer> distancias;
		distancias = MatrixLoader.loadFile(FILE_NAME, SIZE);

		RandomProvider ran;
		System.out.print("Load random from file? (y/N): ");
		Scanner sc = new Scanner(System.in);
		String line = sc.nextLine().toLowerCase().trim();
		if (!line.isEmpty() && line.charAt(0) == 'y') {
			System.out.print("Enter filename: ");
			String file = sc.nextLine().trim();
			FileRandom fr = FileRandom.getInstance();
			fr.loadFile(file);
			ran = fr;
		} else {
			ran = EmbeddedRandom.getInstance();
		}
		sc.close();

		List<Integer> solution = TravelerOperations.firstBest(distancias, ran);
		System.out.println("Solution: " + solution);
		System.out.println("Cost: " + TravelerOperations.calculateCost(solution, distancias));

	}

}
