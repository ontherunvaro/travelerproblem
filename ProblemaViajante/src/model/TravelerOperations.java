package model;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import util.IntegerUtils;
import util.RandomProvider;

/**
 * Operaciones para el algoritmo del viajante
 * 
 * @author �lvaro
 *
 */
public class TravelerOperations {

	private static final Logger log = Logger.getLogger(TravelerOperations.class.getName());
	private static final ConsoleHandler k = new ConsoleHandler();
	private static final DateTimeFormatter df=DateTimeFormatter.ofPattern("yyyyMMddHHssSSS");

	static {
		log.setLevel(Level.ALL);
		k.setLevel(Level.OFF);
		log.addHandler(k);
	}

	/**
	 * Calcula el coste de una soluci�n
	 * 
	 * @param solution
	 *            soluci�n
	 * @param distances
	 *            matriz de distancias entre nodos
	 * @return coste de la soluci�n
	 */
	public static Double calculateCost(List<Integer> solution, LowerTriangularMatrix<Integer> distances) {
		Double ret = 0.0;
		ret += distances.get(solution.get(0) - 1, 0);
		ret += distances.get(solution.get(solution.size() - 1) - 1, 0);
		for (int i = 0; i < solution.size() - 1; i++) {
			int a = solution.get(i);
			int b = solution.get(i + 1);
			Integer dist = distances.get(Math.max(a, b) - 1, Math.min(a, b));
			ret += dist;
		}

		return ret;

	}

	/**
	 * Ejecuta el algoritmo del viajante por el primer mejor
	 * 
	 * @param distances
	 *            matriz de distancias
	 * @param ran
	 *            generador de n�meros aleatorios
	 * @return primer mejor local desde la soluci�n de partida
	 */
	public static List<Integer> firstBest(final LowerTriangularMatrix<Integer> distances, RandomProvider ran) {
		final int size = distances.size() + 1;
		final int neighbor_top = ((size - 1) * (size - 2)) / 2;

		List<Integer> current = IntegerUtils.getRandomArrayFromOne(size - 1, ran);
		Double cost = calculateCost(current, distances);
		LowerTriangularMatrix<Boolean> visitedNeighbors = new LowerTriangularMatrix<>(size - 2, () -> false);

		int swap_count = 0;
		int solutions=0;
		PrintWriter out=null;
		try {
			out = new PrintWriter("traveler"+LocalDateTime.now().format(df)+".txt","utf-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			System.err.println("Cannot open trace file");
			System.exit(-1);
		}

		log.fine("Distance matrix:\n" + distances.toString());
		log.fine("Initial solution: " + current);
		log.fine("Initial cost: " + cost);
		
		out.println("**** SOLUCI�N S_"+solutions+" ****  Recorrido: "+current+"  Coste: "+cost);

		while (swap_count < neighbor_top) {
			List<Integer> temp = new ArrayList<>(current);
			int a, b;

			a = ran.getInt(size - 1);
			b = ran.getInt(size - 1);

			int x = Math.max(a, b);
			int y = Math.min(a, b);
			log.finest("a:" + a + ",b:" + b + ",max:" + x + ",min:" + y);

			while (x == y || visitedNeighbors.get(x - 1, y)) {
				log.finest("Exchange " + x + "," + y + " already visited or invalid");
				y++;
				if ((y >= x)) {
					x++;
					y = 0;
					x %= size - 1;
				}
				log.finest("Trying neighbor " + x + "," + y);
			}

			int tmp = temp.get(x);
			temp.set(x, temp.get(y));
			temp.set(y, tmp);

			
			visitedNeighbors.set(x - 1, y, true);

			log.finer("Checking neighbor number " + swap_count + ": " + temp);

			Double c = calculateCost(temp, distances);
			out.println("VECINO V_"+swap_count+"  \t�ndices: ("+x+","+y+")  \tRecorrido: "+temp+"  \tCoste: "+c);
			swap_count++;
			log.finer("Solution cost: " + c);

			if (c < cost) {
				log.finer("Accepting solution " + temp + " as temporary best");
				current = temp;
				cost = c;
				swap_count = 0;
				solutions++;
				out.println("\n**** SOLUCI�N S_"+solutions+" ****  \tRecorrido: "+current+"  \tCoste: "+cost);				
				visitedNeighbors.init(() -> false);
			} else {
				log.finer("Solution rejected");
			}
		}
		log.fine(current + " is local best. Ending.");

		out.close();
		return current;
	}
}
