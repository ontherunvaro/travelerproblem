package model;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * 
 * Matriz triangular inferior
 * 
 * @author �lvaro
 *
 *
 * @param <E>
 *            tipo de datos
 */
public class LowerTriangularMatrix<E> {

	private List<List<E>> data;

	@SuppressWarnings("unused")
	private LowerTriangularMatrix() {

	}

	/**
	 * @param size
	 *            tama�o de la matriz (n� de filas y columnas en la �ltima fila)
	 */
	public LowerTriangularMatrix(int size) {
		data = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			data.set(i, new ArrayList<>(i + 1));
		}
	}

	/**
	 * @param size
	 *            tama�o de la matriz (n� de filas y columnas en la �ltima fila)
	 * @param initSupplier
	 *            interfaz funcional de inicializaci�n de cada elemento
	 */
	public LowerTriangularMatrix(int size, Supplier<? extends E> initSupplier) {
		data = new ArrayList<>(size);
		for (int i = 0; i < size; i++) {
			ArrayList<E> temp = new ArrayList<>(i + 1);
			for (int k = 0; k < i + 1; k++) {
				temp.add(k, initSupplier.get());
			}

			data.add(i, temp);
		}
	}

	/**
	 * Inicializa todos los elementos a un valor
	 * 
	 * @param initSupplier
	 *            interfaz funcional de inicializaci�n
	 */
	public void init(Supplier<? extends E> initSupplier) {
		for (int i = 0; i < data.size(); i++) {
			for (int k = 0; k < i + 1; k++) {
				this.set(i, k, initSupplier.get());
			}
		}
	}

	/**
	 * Devuelve un valor
	 * 
	 * @param row
	 *            fila
	 * @param column
	 *            columna
	 * @return valor en (row,column)
	 */
	public E get(int row, int column) {
		return data.get(row).get(column);
	}

	/**
	 * Asigna un valor en (row,column)
	 * 
	 * @param row
	 *            fila
	 * @param column
	 *            columna
	 * @param value
	 *            valor a insertar en (row,column)
	 */
	public void set(int row, int column, E value) {
		data.get(row).set(column, value);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		data.stream().forEach(e -> {
			e.stream().forEach(f -> sb.append(f.toString() + " "));
			sb.append("\n");
		});
		return sb.toString();
	}

	/**
	 * Devuelve el n�mero de filas
	 * 
	 * @return n�mero de filas
	 */
	public int size() {
		return data.size();
	}

}
