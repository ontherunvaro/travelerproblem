package util;

/**
 * Interfaz de un generador de n�meros aleatorios
 * 
 * @author �lvaro
 *
 */
public interface RandomProvider {

	/**
	 * Genera un entero aleatorio
	 * 
	 * @param bound
	 *            l�mite superior
	 * @return entero entre <code>0</code> y <code>bound - 1</code>
	 */
	public int getInt(int bound);
}
