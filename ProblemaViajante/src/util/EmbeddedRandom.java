package util;

import java.util.Random;

/**
 * Generador singleton de aleatorios basado en el Random est�ndar
 * 
 * @author �lvaro
 * 
 */
public class EmbeddedRandom implements RandomProvider {

	private Random ran = new Random();

	private static final EmbeddedRandom instance = new EmbeddedRandom();

	private EmbeddedRandom() {
	}

	public static EmbeddedRandom getInstance() {
		return instance;
	}

	@Override
	public int getInt(int bound) {
		return (int) Math.floor(ran.nextFloat() * bound);
	}

	public void setSeed(long seed) {
		ran.setSeed(seed);
	}

}
