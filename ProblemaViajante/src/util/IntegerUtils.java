package util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * Utilidades para trabajar con enteros.
 * 
 * @author �lvaro
 *
 */
public class IntegerUtils {

	private static final Integer INIT_VAL = 0;

	/**
	 * Supplier para el valor por defecto de los enteros
	 * 
	 * @return
	 */
	public static Supplier<Integer> getSupplier() {
		return () -> INIT_VAL;
	}

	/**
	 * Genera una lista aleatoria de enteros no repetidos de <code>0</code> a
	 * <code>size -1</code>
	 * 
	 * @param size
	 *            tama�o de la lista
	 * @param ran
	 *            generador de aleatorios
	 * @return la lista
	 */
	public static List<Integer> getRandomArrayFromZero(int size, RandomProvider ran) {
		return getRandomArray(size, ran, false);
	}

	/**
	 * Genera una lista aleatoria de enteros no repetidos de <code>1</code> a
	 * <code>size</code>
	 * 
	 * @param size
	 *            tama�o de la lista
	 * @param ran
	 *            generador de aleatorios
	 * @return la lista
	 */
	public static List<Integer> getRandomArrayFromOne(int size, RandomProvider ran) {
		return getRandomArray(size, ran, true);
	}

	/**
	 * Genera una lista de enteros en funci�n de los par�metros
	 * 
	 * @param size
	 *            tama�o del array generado
	 * @param ran
	 *            generador de aleatorios
	 * @param fromOne
	 *            si <code>true</code>, genera de 1 a <code>size</code>, si
	 *            <code>false</code>, genera de 0 a <code>size -1</code>
	 * @return la lista
	 */
	private static List<Integer> getRandomArray(int size, RandomProvider ran, boolean fromOne) {
		List<Integer> ret = new ArrayList<>();

		while (ret.size() != size) {
			Integer t = ran.getInt(size);
			if (fromOne)
				t++;
			while (ret.contains(t)) {
				t++;
				if ((fromOne && t > size) || (!fromOne && t > (size - 1)))
					t = 1;
			}
			ret.add(t);
		}

		return ret;
	}

}
