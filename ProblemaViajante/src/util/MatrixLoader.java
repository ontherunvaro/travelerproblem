package util;

import java.io.FileInputStream;
import java.util.Scanner;

import model.LowerTriangularMatrix;

/**
 * Clase utilidad para cargar una matriz de distancias desde un archivo
 * 
 * @author �lvaro
 *
 */
public class MatrixLoader {

	/**
	 * Carga una matriz de distancias desde un archivo
	 * 
	 * @param filename
	 *            nombre del archivo
	 * @param size
	 *            tama�o de la matriz
	 * @return la matriz
	 */
	public static LowerTriangularMatrix<Integer> loadFile(String filename, Integer size) {
		LowerTriangularMatrix<Integer> ret = new LowerTriangularMatrix<>(size - 1, IntegerUtils.getSupplier());

		try {
			Scanner scanner = new Scanner(new FileInputStream(filename));
			for (int i = 0; i < size - 1; i++) {
				String[] values = scanner.nextLine().trim().split("\t");
				for (int k = 0; k < values.length; k++)
					ret.set(i, k, Integer.parseInt(values[k]));
			}
			scanner.close();
		} catch (Exception e) {
			System.out.println("No se puede abrir el archivo de distancias. Cerrando.");
			e.printStackTrace();
			System.exit(1);
		}

		return ret;
	}

}
