package util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * "Generador" singleton de n�meros aleatorios que lee n�meros en el rango [0,1)
 * de un fichero
 * 
 * @author �lvaro
 */
public class FileRandom implements RandomProvider {

	private Queue<Double> data = new LinkedList<>();
	private static final FileRandom instance = new FileRandom();

	private FileRandom() {
	}

	public static FileRandom getInstance() {
		return instance;
	}

	public void loadFile(String file) {
		try {
			Scanner scanner = new Scanner(new FileInputStream(file));
			while (scanner.hasNext())
				data.add(Double.valueOf(scanner.nextLine()));
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	@Override
	public int getInt(int bound) {
		return (int) Math.floor(data.remove() * bound);
	}

	public void emptyQueue() {
		while (!data.isEmpty())
			System.out.println(data.remove());
	}

}
